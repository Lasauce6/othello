package othello.graphics;

import othello.Board;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.StringTokenizer;

public class GamePanel extends JPanel implements ActionListener {
    private final Client client;
    private final Board board;
    private final boolean isAi;
    private int aiPlayer;
    private int aiLevel;

    public GamePanel(Client client, Board board) {
        this.client = client;
        this.board = board;
        this.isAi = false;
    }

    public GamePanel(Client client, Board board, int aiPlayer, int aiLevel) {
        this.client = client;
        this.board = board;
        this.aiPlayer = aiPlayer;
        this.aiLevel = aiLevel;
        isAi = true;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
